package exo4;

import echo.TaskServerEcho;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServeurTcpEchoPool {

    final private int port;
    final private int nbClients;
    final private String quitCommand;

    public ServeurTcpEchoPool(int port, int nbClients, String quitCommand) {
        this.port = port;
        this.nbClients = nbClients;
        this.quitCommand = quitCommand;
    }

    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            ExecutorService pool = Executors.newFixedThreadPool(512);

            for (int i = 0; i < nbClients; ++i) {
                Socket clientSocket = serverSocket.accept();
                pool.execute(new TaskServerEcho(clientSocket, quitCommand));
            }

            serverSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ServeurTcpEchoPool server = new ServeurTcpEchoPool(10007, 10, "quit");
        server.run();
    }
}
