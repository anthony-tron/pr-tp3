package echo;

import java.io.*;
import java.net.Socket;

public class TaskServerEcho implements Runnable {

    private Socket socket;
    private String quitCommand;

    public TaskServerEcho(Socket socket, String quitCommand) {
        this.socket = socket;
        this.quitCommand = quitCommand;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));

            for (;;) {
                String clientMessage = reader.readLine();
                if (clientMessage == null)
                    break; // client closed connection

                writer.write(clientMessage.toUpperCase());
                writer.newLine();
                writer.flush();

                if (clientMessage.compareTo(this.quitCommand) == 0) {
                    break;
                }
            }

            this.socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    } // run()
}
