package exo2;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientTcpEcho {
    private String host;
    private int port;

    public ClientTcpEcho(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void run() {
        try {
            Socket socket = new Socket(host, port);

            Scanner stdin = new Scanner(System.in);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            for (;;) {
                String message = stdin.nextLine();

                writer.write(message);
                writer.newLine();
                writer.flush();

                String response = reader.readLine();
                if (response == null)
                    break;

                System.out.println(response);
            }

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ClientTcpEcho clientTcpEcho = new ClientTcpEcho("127.0.0.1", 10007);
        clientTcpEcho.run();
    }
}
