package exo3;

import echo.TaskServerEcho;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurTcpEchoMulti {

    final private int port;
    final private int nbClients;
    final private String quitCommand;

    public ServeurTcpEchoMulti(int port, int nbClients, String quitCommand) {
        this.port = port;
        this.nbClients = nbClients;
        this.quitCommand = quitCommand;
    }

    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);

            for (int i = 0; i < nbClients; ++i) {
                Socket clientSocket = serverSocket.accept();
                Thread t = new Thread(new TaskServerEcho(clientSocket, quitCommand));
                t.start(); // do not call Thread::run() because it would be executed sequentially
            }

            serverSocket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ServeurTcpEchoMulti server = new ServeurTcpEchoMulti(10007, 10, "quit");
        server.run();
    }
}
