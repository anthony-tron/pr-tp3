package exo1;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurTcpEcho {
    private int port;
    private int nbClients;
    private String quitCommand;

    public ServeurTcpEcho(int port, int nbClients, String quitCommand) {
        this.port = port;
        this.nbClients = nbClients;
        this.quitCommand = quitCommand;
    }

    public void run() {
        ServerSocket serverSocket;

       try {
           serverSocket = new ServerSocket(this.port);

           for (int i = 0; i < this.nbClients; ++i) {
               Socket clientSocket = serverSocket.accept();
               BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
               BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

               for (;;) {
                   String clientMessage = reader.readLine();
                   if (clientMessage == null)
                       break; // client closed connection

                   writer.write(clientMessage.toUpperCase());
                   writer.newLine();
                   writer.flush();

                   if (clientMessage.compareTo(this.quitCommand) == 0) {
                       break;
                   }
               }

               clientSocket.close();
           }

           serverSocket.close();

       } catch (IOException e) {
           e.printStackTrace();
       }
    }

    public static void main(String[] args) {
        ServeurTcpEcho server = new ServeurTcpEcho(10007, 6, "quit");
        server.run();
    }
}
